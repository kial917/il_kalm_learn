declare module '*.css' {
    interface Style {
        wrapper: any;
        [key: string]: string;
            }
        
        const style: Style;

        export default style;
}