import React from 'react';


import style from './style.css';
import { LabeledInput, Button, ButtonColors } from '@main/components/';


class Navbar extends React.Component {

    render() {
        return (
            <div className={style.wrapper}>
                <form className={style.navbar}>

                    <header>
                        <h3>Psycholog.kazan</h3>
                    </header>
                    <div className={style.right}>
                        <div className={style.navtitles}>
                            <h5>Выбрать время</h5>
                            <h5>Узнай себя</h5>
                        </div>
                        <div>
                            <Button type="submit" className={style.submitButton} colorScheme={ButtonColors.blue} children='Ввести'></Button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default Navbar;