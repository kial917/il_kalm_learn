import React from 'react';
/*import styled from 'styled-components'

export enum COLORS {
    MAIN =`#378052`,
}

const Wrapper =styled.div`
background-color:${COLORS.MAIN};

`;*/

import style from './style.css';
import {LabeledInput, Button, ButtonColors } from '@main/components/';

console.log(style);


console.log('1');
console.log('1');
console.log('1');

class Login extends React.Component {
    firstInputRef= React.createRef <HTMLInputElement>();

  


    componentDidMount(){
        this.firstInputRef.current.focus();
    }

    render () {
        return (
            <div className={style.wrapper}>
            <form className={style.loginForm}>
                    <LabeledInput inputRef={this.firstInputRef} label="Введите логин" id="login-input" name='login' />
                    <LabeledInput label="Введите пароль" id="password-input" name='password' type='password'/>
                    <Button type="submit" className={style.submitButton} colorScheme={ButtonColors.blue} children='Ввести'></Button>

            </form>
        </div>
        )
    }
}

export default Login;