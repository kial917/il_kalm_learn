import React from 'react';


import Login from './containers/login';
import Navbar from './containers/mainpage';
import './app.css';

const App = () => (

    <div>
        <div>
            <Navbar/>
        </div>
        <div>
            <Login />
        </div>
    </div>

)

export default App