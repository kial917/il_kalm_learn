import React from 'react';
import ReactDom from 'react-dom';

import App from './app'
//const App = () => <h1>Hello world для проекта - il_kalm_learn</h1>

export default () => <App/>;

export const mount = (Сomponent) => {
    ReactDom.render(
        <Сomponent/>,
        document.getElementById('app')
    );
};

export const unmount = () => {
    ReactDom.unmountComponentAtNode(document.getElementById('app'));
};

