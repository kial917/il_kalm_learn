import React from 'react';
import cn from 'classnames';
import { ButtonColors} from './index'

import style from './style.css';
import { string } from 'prop-types';

interface ButtonProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>{
        colorScheme: ButtonColors;
        onClick?: ()=>void;
        className?: string;

}

const Button: React.FC<ButtonProps> =({colorScheme, children, className, ...rest })=>(
 
        <button 
            {...rest}
            className={cn(style.main, style[colorScheme], className)}
        >
            {children}
        </button>
    
   
);
export default Button;