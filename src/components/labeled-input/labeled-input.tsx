import React, { HTMLAttributes } from 'react'
import react from 'react'
import PropTypes from 'prop-types'

import style from './style.css';

interface LabeledInputProps extends Omit<HTMLAttributes<HTMLInputElement>, 'id'|'name'>{
    id: string | number;
    name: string;
    label: string;
    type?: string;
    inputRef?: React.RefObject<HTMLInputElement>;

}

export const LabeledInput: React.FC<LabeledInputProps> = ({inputRef, label, id, name, type}, ...rest) => (
    <div className={style.wrapper}>
        <label className={style.label} htmlFor={String(id)}>{label}</label>
        <input className={style.field} ref={inputRef} type={type} name={name} id={String(id)} {...rest}/>
    </div>

)

LabeledInput.propTypes = {
    label: (props, propName)=>{
        const value = props[propName];

        if (value.split(/\s/).length !== 2){
            return new Error(`Длина поля ${propName} не равна 2м`)
        }
    },
    id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ]),
    name: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['text','password']) 
}

LabeledInput.defaultProps = {
    type: 'text'
}